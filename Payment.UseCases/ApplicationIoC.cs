﻿using Payment.Application.Orders;

namespace Microsoft.Extensions.DependencyInjection;

public static class ApplicationIoC
{
    public static IServiceCollection AddApplicationIoC(this IServiceCollection services)
    {
        if (services is null)
            throw new ArgumentNullException(nameof(services));

        services.AddScoped<IGetOrderById, GetOrderById>();
        services.AddScoped<IUpdateOrder, UpdateOrder>();
        services.AddScoped<ICreateOrder, CreateOrder>();

        return services;
    }
}
