﻿using Order = Payment.Domain.Orders.Orders;

namespace Payment.Application.Orders;

public interface IOrderRepository
{
    Task<Order?> GetOrderById(Guid id, bool asNoTracking = true);
    Task Save(Order orders);
}
