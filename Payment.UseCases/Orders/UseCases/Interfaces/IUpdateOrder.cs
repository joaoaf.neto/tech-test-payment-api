﻿namespace Payment.Application.Orders;

public interface IUpdateOrder
{
    Task<UpdateOrderResponse> Execute(UpdateOrderRequest request);
}
