﻿namespace Payment.Application.Orders;

public interface IGetOrderById
{
    Task<GetOrderByIdResponse> Execute(Guid id);
}
