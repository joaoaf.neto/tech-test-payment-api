﻿namespace Payment.Application.Orders;

public interface ICreateOrder
{
    Task<CreateOrderResponse> Execute(CreateOrderRequest request);
}
