﻿using AutoMapper;
using Payment.Domain.Orders;
using Payment.Domain.Validators;
using Payment.UseCases.UoW;

namespace Payment.Application.Orders;

public class CreateOrder : ICreateOrder
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;

    public CreateOrder(IOrderRepository orderRepository,
                       IMapper mapper,
                       IUnitOfWork unitOfWork)
    {
        _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
    }

    public async Task<CreateOrderResponse> Execute(CreateOrderRequest request)
    {
        if (request is null)
            throw new OrderException(OrderError.UninformedData);

        ValidationHelper.Validate(request);

        var model = _mapper.Map<Domain.Orders.Orders>(request);

        await _unitOfWork.ExecuteInTransaction(async () =>
        {
            await _orderRepository.Save(model);
        });

        return _mapper.Map<CreateOrderResponse>(model);
    }
}
