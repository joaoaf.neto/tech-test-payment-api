﻿using AutoMapper;
using Payment.Domain.Orders;
using Payment.Domain.Validators;
using Payment.UseCases.UoW;

namespace Payment.Application.Orders;

public class UpdateOrder : IUpdateOrder
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;

    public UpdateOrder(IOrderRepository orderRepository,
                       IMapper mapper,
                       IUnitOfWork unitOfWork)
    {
        _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
    }

    public async Task<UpdateOrderResponse> Execute(UpdateOrderRequest request)
    {
        if (request is null)
            throw new UpdateOrderException(UpdateOrderError.UninformedData);

        ValidationHelper.Validate(request);

        var order = await _orderRepository.GetOrderById(request.Id, false)
            ?? throw new OrderNotFoundException(OrderNotFoundError.ById);

        var allowedTransaction = OrderOperations.AllowedTransactionMap.TryGetValue(order.Status, out var allowedTransactionResult)
            ? allowedTransactionResult : throw new UpdateOrderException(UpdateOrderError.NotUpdatable);

        if (!allowedTransaction.Contains(request.Status))
            throw new UpdateOrderException(UpdateOrderError.UnauthorizedTransaction);

        order.Status = request.Status;
        order.UpdatedAt = DateTime.UtcNow;

        await _unitOfWork.ExecuteInTransaction(async () =>
        {
            await _orderRepository.Save(order);
        });

        return _mapper.Map<UpdateOrderResponse>(order);
    }
}
