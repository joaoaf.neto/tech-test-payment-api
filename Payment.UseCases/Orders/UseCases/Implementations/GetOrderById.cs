﻿using AutoMapper;
using Payment.Domain.Orders;

namespace Payment.Application.Orders;

public class GetOrderById : IGetOrderById
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;

    public GetOrderById(IOrderRepository orderRepository,
                        IMapper mapper)
    {
        _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public async Task<GetOrderByIdResponse> Execute(Guid id)
    {
        if (id == Guid.Empty)
            throw new OrderException(OrderError.UninformedId);

        var result = await _orderRepository.GetOrderById(id) ?? throw new OrderNotFoundException(OrderNotFoundError.ById);

        return _mapper.Map<GetOrderByIdResponse>(result);
    }
}
