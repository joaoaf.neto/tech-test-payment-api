﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Application.Orders;

public record CreateOrderRequest
{
    [Required]
    public SellerRequest Seller { get; set; } = default!;
    [Required, MinLength(1)]
    public IEnumerable<ItemRequest> Items { get; set; } = Enumerable.Empty<ItemRequest>();
}
