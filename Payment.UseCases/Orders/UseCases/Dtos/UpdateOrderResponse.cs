﻿using Payment.Domain.Orders;

namespace Payment.Application.Orders;

public record UpdateOrderResponse
{
    public OrderStatus Status { get; set; }
}
