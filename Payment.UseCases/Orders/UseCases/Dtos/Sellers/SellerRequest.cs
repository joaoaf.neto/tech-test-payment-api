﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Application.Orders;

public record SellerRequest
{
    [Required, MaxLength(100)]
    public string Name { get; set; } = string.Empty;
    [Required, MaxLength(100)]
    public string Email { get; set; } = string.Empty;
    [Required, MaxLength(16)]
    public string PhoneNumber { get; set; } = string.Empty;
    [Required, MaxLength(11)]
    public string Document { get; set; } = string.Empty;
}
