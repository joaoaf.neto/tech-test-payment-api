﻿using Payment.Domain.Orders;

namespace Payment.Application.Orders;

public record GetOrderByIdResponse
{
    public Guid Id { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    public OrderStatus Status { get; set; }
    public SellerResponse Seller { get; set; } = default!;
    public IEnumerable<ItemsResponse> Items { get; set; } = Enumerable.Empty<ItemsResponse>();
}
