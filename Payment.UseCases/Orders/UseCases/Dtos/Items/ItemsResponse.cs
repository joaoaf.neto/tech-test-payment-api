﻿namespace Payment.Application.Orders;

public record ItemsResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
}
