﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Application.Orders;

public record ItemRequest
{
    [Required, MaxLength(250)]
    public string Name { get; set; } = string.Empty;
}
