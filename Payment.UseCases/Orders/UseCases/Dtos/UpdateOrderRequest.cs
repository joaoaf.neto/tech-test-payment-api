﻿using Payment.Domain.Orders;
using Payment.Domain.Validators;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Payment.Application.Orders;

public record UpdateOrderRequest
{
    [Required, JsonIgnore, RequiredNonDefault]
    public Guid Id { get; set; }
    [Required, ValidEnumValue]
    public OrderStatus Status { get; set; }
}
