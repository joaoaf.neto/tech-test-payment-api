﻿using AutoMapper;
using Payment.Domain.Orders;

namespace Payment.Application.Orders;

public class SellerProfile : Profile
{
    public SellerProfile()
    {
        CreateMap<Sellers, SellerResponse>();

        CreateMap<SellerRequest, Sellers>();
    }
}
