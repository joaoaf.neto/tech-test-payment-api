﻿using AutoMapper;
using Order = Payment.Domain.Orders.Orders;

namespace Payment.Application.Orders;

public class OrderProfile : Profile
{
    public OrderProfile()
    {
        CreateMap<Order, GetOrderByIdResponse>();
        CreateMap<Order, CreateOrderResponse>();
        CreateMap<Order, UpdateOrderResponse>();

        CreateMap<CreateOrderRequest, Order>();
        CreateMap<UpdateOrderRequest, Order>();
    }
}
