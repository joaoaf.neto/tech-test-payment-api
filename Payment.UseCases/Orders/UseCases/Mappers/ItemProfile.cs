﻿using AutoMapper;
using Payment.Domain.Orders;

namespace Payment.Application.Orders;

public class ItemProfile : Profile
{
    public ItemProfile()
    {
        CreateMap<Items, ItemsResponse>();

        CreateMap<ItemRequest, Items>();
    }
}
