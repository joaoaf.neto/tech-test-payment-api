﻿namespace Payment.UseCases.UoW;

public interface IUnitOfWork
{
    Task<T> ExecuteInTransaction<T>(Func<Task<T>> work);
    Task ExecuteInTransaction(Func<Task> work);
}
