﻿using System.Runtime.Serialization;

namespace Payment.Domain.Exceptions;

public abstract class DomainException : Exception
{
    public abstract string Key { get; }
    public List<DomainError> Errors { get; set; } = new();

    protected DomainException(string message) : base(message) { }

    protected DomainException(SerializationInfo info, StreamingContext context) : base(info, context) { }
}

public abstract class DomainException<T> : DomainException where T : DomainError
{
    protected DomainException() : base("A business error occurred, check the 'errors' property for details.") { }

    protected DomainException(string message) : base(message) { }

    protected DomainException(SerializationInfo info, StreamingContext context) : base(info, context) { }

    public DomainException AddError(params T[] errors)
    {
        if (errors is not null && errors.Any())
            Errors.AddRange(errors);

        return this;
    }
}
