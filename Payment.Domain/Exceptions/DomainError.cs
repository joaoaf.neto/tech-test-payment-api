﻿namespace Payment.Domain.Exceptions;

public abstract class DomainError
{
    public string Key { get; }
    public string Message { get; }

    protected DomainError(string key, string messge)
    {
        Key = key;
        Message = messge;
    }
}
