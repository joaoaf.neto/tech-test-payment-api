﻿using System.Runtime.Serialization;

namespace Payment.Domain.Exceptions;

public interface INotFoundException { }

public abstract class NotFoundException<T> : DomainException<T>, INotFoundException where T : DomainError
{
    protected NotFoundException() : base() { }

    protected NotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) { }
}