﻿using Payment.Domain.Exceptions;
using System.Runtime.Serialization;

namespace Payment.Domain.Validators;

[Serializable]
public class ModelValidationException : DomainException<DomainError>
{
    public override string Key => nameof(ModelValidationException);

    public ModelValidationException(params DomainError[] errors) : base() => AddError(errors);

    public ModelValidationException(string message) : base(message) { }

    protected ModelValidationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
}

public class ModelValidationError : DomainError
{
    public ModelValidationError(string key, string message) : base(key, message) { }
}