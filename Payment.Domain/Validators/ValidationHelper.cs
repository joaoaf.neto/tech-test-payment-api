﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Validators;

public static class ValidationHelper
{
    public static void Validate<T>(T model) where T : class
    {
        if (model is null)
            throw new ArgumentNullException(nameof(model));

        if (!ModelValidator.TryValidate(model, out IEnumerable<ValidationResult> errors))
            throw new ModelValidationException(errors.Select(e => new ModelValidationError(e.MemberNames.First(), e.ErrorMessage!)).ToArray());
    }

    public static IEnumerable<string> GetErrors<T>(T model) where T : class
    {
        if (!ModelValidator.TryValidate(model, out IEnumerable<ValidationResult> errors))
            return errors.Select(e => e.ErrorMessage!);

        return Enumerable.Empty<string>();
    }
}
