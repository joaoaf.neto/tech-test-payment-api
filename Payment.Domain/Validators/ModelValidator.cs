﻿using RecursiveDataAnnotationsValidation;
using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Validators;

public static class ModelValidator
{
    public static bool TryValidate<T>(T model, out IEnumerable<ValidationResult> errors)
    {
        if (model is null)
            throw new ArgumentNullException(nameof(model));

        var validationResults = new List<ValidationResult>();
        var validator = new RecursiveDataAnnotationValidator();
        var isValid = validator.TryValidateObjectRecursive(model, validationResults);

        errors = validationResults;
        return isValid;
    }
}
