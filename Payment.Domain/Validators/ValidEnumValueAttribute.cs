﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Validators;

public class ValidEnumValueAttribute : ValidationAttribute
{
    public override bool IsValid(object? value)
    {
        if (value is null)
            return true;

        return value is Enum enumValue && enumValue.IsValid();
    }

    public override string FormatErrorMessage(string name)
    {
        return $"Invalid value for field {name}";
    }
}
