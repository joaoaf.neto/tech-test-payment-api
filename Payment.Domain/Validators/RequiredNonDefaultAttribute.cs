﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Validators;

public class RequiredNonDefaultAttribute : RequiredAttribute
{
    public override bool IsValid(object? value) => base.IsValid(value);
}
