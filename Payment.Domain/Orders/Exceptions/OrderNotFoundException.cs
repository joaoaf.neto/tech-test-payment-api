﻿using Payment.Domain.Exceptions;
using System.Runtime.Serialization;

namespace Payment.Domain.Orders;
[Serializable]
public sealed class OrderNotFoundException : NotFoundException<OrderNotFoundError>
{
    public override string Key => nameof(OrderNotFoundException);
    public OrderNotFoundException(OrderNotFoundError error) => AddError(error);

    private OrderNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) { }
}

public sealed class OrderNotFoundError : DomainError
{
    private OrderNotFoundError(string key, string message) : base(key, message) { }

    public static OrderNotFoundError ById => new(nameof(ById), "No order was found with the given id.");
}
