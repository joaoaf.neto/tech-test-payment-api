﻿using Payment.Domain.Exceptions;
using System.Runtime.Serialization;

namespace Payment.Domain.Orders;
[Serializable]
public sealed class OrderException : DomainException<OrderError>
{
    public override string Key => nameof(OrderException);
    public OrderException(OrderError errors) => AddError(errors);

    private OrderException(SerializationInfo info, StreamingContext context) : base(info, context) { }
}

public sealed class OrderError : DomainError
{
    public OrderError(string key, string message) : base(key, message) { }
    public static OrderError UninformedData => new(nameof(UninformedData), "No data has been reported");
    public static OrderError UninformedId => new(nameof(UninformedId), "No Id has been reported");
}
