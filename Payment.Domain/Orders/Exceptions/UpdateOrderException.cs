﻿using Payment.Domain.Exceptions;
using System.Runtime.Serialization;

namespace Payment.Domain.Orders;
[Serializable]
public sealed class UpdateOrderException : DomainException<UpdateOrderError>
{
    public override string Key => nameof(UpdateOrderException);
    public UpdateOrderException(UpdateOrderError errors) => AddError(errors);

    private UpdateOrderException(SerializationInfo info, StreamingContext context) : base(info, context) { }
}

public sealed class UpdateOrderError : DomainError
{
    private UpdateOrderError(string key, string message) : base(key, message) { }

    public static UpdateOrderError UninformedData 
        => new(nameof(UninformedData), "No data has been reported");
    public static UpdateOrderError NotUpdatable 
        => new(nameof(NotUpdatable), "The current status of your order cannot be updated");
    public static UpdateOrderError UnauthorizedTransaction 
        => new(nameof(UnauthorizedTransaction), "The current status of your order cannot be updated to the desired status.");
}
