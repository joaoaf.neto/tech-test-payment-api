﻿namespace Payment.Domain.Orders;

public static class OrderOperations
{
    public static readonly Dictionary<OrderStatus, IEnumerable<OrderStatus>> AllowedTransactionMap = new()
    {
        {OrderStatus.PaymentPending, new[]{OrderStatus.PaymentApproved, OrderStatus.Cancelled} },
        {OrderStatus.PaymentApproved, new[]{OrderStatus.ShippedCarrier, OrderStatus.Cancelled} },
        {OrderStatus.ShippedCarrier, new[]{OrderStatus.Delivered} }
    };
}
