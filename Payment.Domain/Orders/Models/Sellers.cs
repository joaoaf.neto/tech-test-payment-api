﻿using System.Collections.ObjectModel;

namespace Payment.Domain.Orders;

public record Sellers
{
    public Guid Id { get; set; } = Guid.Empty;
    public string Document { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public string PhoneNumber { get; set; } = string.Empty;

    public ICollection<Orders> Orders { get; set; } = new Collection<Orders>();
}
