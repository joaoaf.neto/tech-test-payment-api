﻿namespace Payment.Domain.Orders;

public record Items
{
    public Guid Id { get; set; } = Guid.Empty;
    public string Name { get; set; } = string.Empty;

    public Guid OrderId { get; set; }
    public Orders Order { get; set; } = default!;
}
