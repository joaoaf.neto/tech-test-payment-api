﻿using System.Collections.ObjectModel;

namespace Payment.Domain.Orders;

public record Orders
{
    public Guid Id { get; set; } = Guid.Empty;
    public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
    public DateTime UpdatedAt { get; set; } = DateTime.UtcNow;
    public OrderStatus Status { get; set; } = OrderStatus.PaymentPending;

    public Guid SellerId { get; set; }
    public Sellers Seller { get; set; } = default!;

    public ICollection<Items> Items { get; set; } = new Collection<Items>();
}

public enum OrderStatus
{
    PaymentPending = 1,
    PaymentApproved,
    ShippedCarrier,
    Delivered,
    Cancelled
}
