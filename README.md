# DOCUMENTAÇÃO

## Tecnologias utilizadas

- .NET 7.0
  - AutoMapper (para mapeamento)
  - Data Annotations: Model Validation (para validação de requisições)
  - XUnit/Moq/FluentAssertions (Para testes)
- Clean Architecture com uma estrutura baseada no Hexagonal utilizando DDD
- Entity Framework Core + SQLite para persistência de dados (foi mais conveniente utilizar uma estrutura de persistência)

## Detalhes de implementação

Será necessário rodar o comando Update-Database no Packafe Manager Console com o Default project (Projeto padrão) Payment.Repositories, a aplicação criará um arquivo .db em sua pasta raiz (pasta 'bin' do projeto Api durante a execução) assim que for executada, servindo como banco de dados.

Não foram realizados testes para todos os elementos da aplicação, considerando elementos estruturais baseados fortemente em elementos do próprio framework como mais robustos, apenas criando testes para aqueles considerados mais críticos com relação às regras negociais. (Application)

## Registro de funcionamento

### Registrar venda com erros na requisição:

![Erro ao registrar venda](.docs/venda-erro.png)

### Registrar venda com sucesso:

![Registrar venda](.docs/venda-sucesso.png)

### Erro ao buscar venda por id:

![Erro ao buscar venda por id](.docs/obter-venda-erro.png)

### Buscar venda por id:

![Buscar venda por id](.docs/obter-venda-sucesso.png)

### Erro ao atualizar status da venda:

![Payment Pending para Payment Pending](.docs/atualiza-status-erro.png)

### Atualizando para status entregue:

![Atualizando status de ShippedCarrier para Delivered](.docs/atualizacao-status-transporte-entregue.png)

### Erro ao atualizar status da venda:

![Tentativa de atualizar o status Delivered](.docs/atualiza-status-iguais-erro.png)
## Testes Unitários:

![Testes](.docs/tests.png)

## INSTRUÇÕES PARA O TESTE TÉCNICO

- Crie um fork deste projeto (https://gitlab.com/Pottencial/tech-test-payment-api/-/forks/new). É preciso estar logado na sua conta Gitlab;
- Adicione @Pottencial (Pottencial Seguradora) como membro do seu fork. Você pode fazer isto em https://gitlab.com/`your-user`/tech-test-payment-api/settings/members;
- Quando você começar, faça um commit vazio com a mensagem "Iniciando o teste de tecnologia" e quando terminar, faça o commit com uma mensagem "Finalizado o teste de tecnologia";
- Commit após cada ciclo de refatoração pelo menos;
- Não use branches;
- Você deve prover evidências suficientes de que sua solução está completa indicando, no mínimo, que ela funciona;

## O TESTE

- Construir uma API REST utilizando .Net Core, Java ou NodeJs (com Typescript);
- A API deve expor uma rota com documentação swagger (http://.../api-docs).
- A API deve possuir 3 operações:
  1. Registrar venda: Recebe os dados do vendedor + itens vendidos. Registra venda com status "Aguardando pagamento";
  2. Buscar venda: Busca pelo Id da venda;
  3. Atualizar venda: Permite que seja atualizado o status da venda.
     - OBS.: Possíveis status: `Pagamento aprovado` | `Enviado para transportadora` | `Entregue` | `Cancelada`.
- Uma venda contém informação sobre o vendedor que a efetivou, data, identificador do pedido e os itens que foram vendidos;
- O vendedor deve possuir id, cpf, nome, e-mail e telefone;
- A inclusão de uma venda deve possuir pelo menos 1 item;
- A atualização de status deve permitir somente as seguintes transições:
  - De: `Aguardando pagamento` Para: `Pagamento Aprovado`
  - De: `Aguardando pagamento` Para: `Cancelada`
  - De: `Pagamento Aprovado` Para: `Enviado para Transportadora`
  - De: `Pagamento Aprovado` Para: `Cancelada`
  - De: `Enviado para Transportador`. Para: `Entregue`
- A API não precisa ter mecanismos de autenticação/autorização;
- A aplicação não precisa implementar os mecanismos de persistência em um banco de dados, eles podem ser persistidos "em memória".

## PONTOS QUE SERÃO AVALIADOS

- Arquitetura da aplicação - embora não existam muitos requisitos de negócio, iremos avaliar como o projeto foi estruturada, bem como camadas e suas responsabilidades;
- Programação orientada a objetos;
- Boas práticas e princípios como SOLID, DDD (opcional), DRY, KISS;
- Testes unitários;
- Uso correto do padrão REST;

