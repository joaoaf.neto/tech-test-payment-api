﻿using Microsoft.AspNetCore.Mvc;
using Payment.Application.Orders;

namespace Payment.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class OrdersController : Controller
{
    private readonly ICreateOrder _createOrder;
    private readonly IGetOrderById _getOrderById;
    private readonly IUpdateOrder _updateOrder;

    public OrdersController(ICreateOrder createOrder,
                           IGetOrderById getOrderById,
                           IUpdateOrder updateOrder)
    {
        _createOrder = createOrder ?? throw new ArgumentNullException(nameof(createOrder));
        _getOrderById = getOrderById ?? throw new ArgumentNullException(nameof(getOrderById));
        _updateOrder = updateOrder ?? throw new ArgumentNullException(nameof(updateOrder));
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetOrderByIdResponse))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ProblemDetails))]
    public async Task<IActionResult> GetOrderById(Guid id) => Ok(await _getOrderById.Execute(id));

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(CreateOrderResponse))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ProblemDetails))]
    public async Task<IActionResult> CreateOrder(CreateOrderRequest request) => Created(string.Empty, await _createOrder.Execute(request));

    [HttpPatch("{id}/status")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UpdateOrderResponse))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ProblemDetails))]
    public async Task<IActionResult> UpdateOrder(Guid id, [FromBody] UpdateOrderRequest request)
        => Ok(await _updateOrder.Execute(request with { Id = id }));
}
