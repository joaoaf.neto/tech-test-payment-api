﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Payment.Domain.Exceptions;

namespace Payment.Api.Filters;

public class GlobalExceptionFilter : IExceptionFilter
{
    private readonly ILogger<GlobalExceptionFilter> _logger;

    public GlobalExceptionFilter(ILogger<GlobalExceptionFilter> logger)
    {
        _logger = logger;
    }

    public void OnException(ExceptionContext context)
    {
        var result = context.Exception switch
        {
            INotFoundException => DomainExceptionHandler(context, StatusCodes.Status404NotFound),
            DomainException => DomainExceptionHandler(context, StatusCodes.Status400BadRequest),
            _ => InternalServerErrorExceptionHandler(context)
        };

        if (result is not null)
        {
            context.Result = result;
            context.ExceptionHandled = true;
        }
    }

    private ObjectResult DomainExceptionHandler(ExceptionContext context, int statusCode)
    {
        var ex = context.Exception as DomainException;
        var problemDetails = CreateProblemDetails(context, ex!.Key);
        problemDetails.Status = statusCode;

        var errors = ex.Errors
            .GroupBy(e => e.Key, StringComparer.OrdinalIgnoreCase)
            .ToDictionary(e => e.Key, e => e.First().Message);

        if (_logger.IsEnabled(LogLevel.Information))
            _logger.LogWarning(ex, "[BorderException] {Title}: {Errors}", problemDetails.Title, errors);

        if (errors.Any())
            problemDetails.Extensions["errors"] = errors;

        if (ex.Data.Count > 0)
            problemDetails.Extensions["data"] = context.Exception.Data;

        return new ObjectResult(problemDetails);
    }

    private ObjectResult InternalServerErrorExceptionHandler(ExceptionContext context)
    {
        var problemDetails = CreateProblemDetails(context);
        problemDetails.Status = StatusCodes.Status500InternalServerError;

        _logger.LogError(context.Exception,
            "[Exception] {Title}: {Message}",
            problemDetails.Title,
            context.Exception.Message);

        return new ObjectResult(problemDetails);
    }

    private static ProblemDetails CreateProblemDetails(ExceptionContext context, string? title = null)
    {
        var problemDetails = new ProblemDetails
        {
            Title = title ?? context.Exception.GetType().Name,
            Instance = context.HttpContext.Request.Path.Value,
            Detail = context.Exception.Message
        };

        problemDetails.Extensions["exception"] = context.Exception.ToString();

        return problemDetails;
    }
}
