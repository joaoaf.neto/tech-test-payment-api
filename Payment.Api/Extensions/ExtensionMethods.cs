﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Payment.Application.Orders;
using Payment.Repositories;

namespace Payment.Api.Extensions;

public static class ExtensionMethods
{
    public static void ConfigureAutoMapper(this IMapperConfigurationExpression cfg)
    {
        cfg.AddProfile<OrderProfile>();
        cfg.AddProfile<SellerProfile>();
        cfg.AddProfile<ItemProfile>();
    }

    public static void ConfigureDependencyInjection(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

        services.AddScoped<IUrlHelper, UrlHelper>(
            implementationFactory =>
            {
                var actionContext = implementationFactory.GetService<IActionContextAccessor>()?.ActionContext
                ?? throw new ArgumentNullException(nameof(implementationFactory));

                return new UrlHelper(actionContext);
            });

        services.AddApplicationIoC()
            .AddRepositoryIoC(configuration.SafeGet<RepositorySettings>());
    }
}
