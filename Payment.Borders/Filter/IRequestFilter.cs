﻿using Payment.Borders.UseCases;

namespace Payment.Borders.Filter;

public interface IRequestFilter
{
    Task<TResponse> Send<TResponse>(IRequest<TResponse> request);
}
