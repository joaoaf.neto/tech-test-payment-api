﻿using Microsoft.EntityFrameworkCore;

namespace Payment.Borders.Repositories;

public interface IRepositoryContextManager<TContext> where TContext : DbContext
{
    TContext GetContext();
}
