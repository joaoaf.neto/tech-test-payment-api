﻿using Payment.Borders.Entities;

namespace Payment.Borders.Repositories;

public interface IRepository<TEntity> where TEntity : StoredEntity
{
    Task<TEntity?> GetById(Guid id, Func<IQueryable<TEntity>, IQueryable<TEntity>>? queryExtensions = null);
    Task Add(TEntity entity);
    Task<TEntity?> Update(TEntity entity);
}
