﻿namespace Payment.Borders.Entities;

public abstract record StoredEntity
{
    public Guid Id { get; init; } = Guid.NewGuid();
    public DateTime CreatedAt { get; init; } = DateTime.UtcNow;
}
