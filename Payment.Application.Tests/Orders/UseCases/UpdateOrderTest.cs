﻿using AutoMapper;
using Moq;
using Payment.Application.Orders;
using Payment.Domain.Orders;
using Payment.Domain.Validators;
using Payment.UseCases.UoW;
using Order = Payment.Domain.Orders.Orders;

namespace Payment.Application.Tests.Orders.UseCases;

public class UpdateOrderTest
{
    private readonly Mock<IMapper> _mapper;
    private readonly Mock<IOrderRepository> _orderRepository;
    private readonly Mock<IUnitOfWork> _unitOfWork;
    private readonly IUpdateOrder _updateOrder;

    public UpdateOrderTest()
    {
        _unitOfWork = new();
        _mapper = new();
        _orderRepository = new();
        _updateOrder = new UpdateOrder(_orderRepository.Object, _mapper.Object, _unitOfWork.Object);
    }

    [Fact]
    public async Task UpdateOrder_NullInput_ThrowsException()
    {
        var ex = await Assert.ThrowsAsync<UpdateOrderException>(() => _updateOrder.Execute(null));

        Assert.Single(ex.Errors);
        Assert.Equal(nameof(UpdateOrderException), ex.Key);
        Assert.Equal(nameof(UpdateOrderError.UninformedData), ex.Errors[0].Key);

        VerifyNoOtherCalls();
    }

    [Fact]
    public async Task UpdateOrder_OrderDoesntExist_ThrowsException()
    {
        Order? order = null;

        _orderRepository.Setup(x => x.GetOrderById(It.IsAny<Guid>(), It.IsAny<bool>())).ReturnsAsync(order);

        var ex = await Assert.ThrowsAsync<OrderNotFoundException>(() => _updateOrder.Execute(RequestMock));

        Assert.Single(ex.Errors);
        Assert.Equal(nameof(OrderNotFoundException), ex.Key);

        _orderRepository.Verify(x => x.GetOrderById(It.IsAny<Guid>(), It.IsAny<bool>()), Times.Once);

        VerifyNoOtherCalls();
    }

    [Fact]
    public async Task UpdateOrder_MissingRequiredProperties_ThrowsException()
    {
        var ex = await Assert.ThrowsAsync<ModelValidationException>(() => _updateOrder.Execute(new()));

        Assert.Single(ex.Errors);
        Assert.Equal(nameof(ModelValidationException), ex.Key);

        VerifyNoOtherCalls();
    }

    [Theory]
    [InlineData(OrderStatus.Delivered, OrderStatus.PaymentPending)]
    [InlineData(OrderStatus.Cancelled, OrderStatus.PaymentPending)]
    [InlineData(OrderStatus.PaymentApproved, OrderStatus.PaymentPending)]
    [InlineData(OrderStatus.ShippedCarrier, OrderStatus.PaymentApproved)]
    [InlineData(OrderStatus.PaymentPending, OrderStatus.ShippedCarrier)]
    [InlineData(OrderStatus.Delivered, OrderStatus.ShippedCarrier)]
    public async Task UpdateOrder_InvalidTransactions_ThrowsException(OrderStatus orderStatusDb, OrderStatus requestStatus)
    {
        _orderRepository.Setup(x => x.GetOrderById(It.IsAny<Guid>(), It.IsAny<bool>())).ReturnsAsync(new Order { Status = orderStatusDb });

        var ex = await Assert.ThrowsAsync<UpdateOrderException>(() => _updateOrder.Execute(RequestMock with { Status = requestStatus }));

        Assert.Single(ex.Errors);
        Assert.Equal(nameof(UpdateOrderException), ex.Key);

        _orderRepository.Verify(x => x.GetOrderById(It.IsAny<Guid>(), It.IsAny<bool>()), Times.Once);

        VerifyNoOtherCalls();
    }

    [Theory]
    [MemberData(nameof(OrdersStatusAllowed))]
    public async Task UpdateOrder_TransactionsAllowed_Success(OrderStatus orderStatusDb, OrderStatus requestStatus)
    {
        _orderRepository.Setup(x => x.GetOrderById(It.IsAny<Guid>(), It.IsAny<bool>())).ReturnsAsync(new Order { Status = orderStatusDb });
        _mapper.Setup(x => x.Map<UpdateOrderResponse>(It.IsAny<Order>())).Returns(new UpdateOrderResponse { Status = requestStatus });
        _unitOfWork.Setup(x => x.ExecuteInTransaction(It.IsAny<Func<Task>>())).Returns(async (Func<Task> work) => { await work(); });
        _orderRepository.Setup(x => x.Save(It.IsAny<Order>())).Verifiable();

        var response = await _updateOrder.Execute(RequestMock with { Status = requestStatus });

        Assert.Equal(requestStatus, response.Status);

        _orderRepository.Verify(x => x.Save(It.Is<Order>(o => o.Status == requestStatus)), Times.Once);
        _unitOfWork.Verify(x => x.ExecuteInTransaction(It.IsAny<Func<Task>>()), Times.Once);
        _mapper.Verify(x => x.Map<UpdateOrderResponse>(It.IsAny<Order>()), Times.Once);
        _orderRepository.Verify(x => x.GetOrderById(It.IsAny<Guid>(), It.IsAny<bool>()), Times.Once);

        VerifyNoOtherCalls();
    }

    private void VerifyNoOtherCalls()
    {
        _orderRepository.VerifyNoOtherCalls();
        _unitOfWork.VerifyNoOtherCalls();
        _mapper.VerifyNoOtherCalls();
    }

    private static UpdateOrderRequest RequestMock =>
        new()
        {
            Id = Guid.NewGuid(),
            Status = OrderStatus.PaymentPending
        };

    public static IEnumerable<object[]> OrdersStatusAllowed
    {
        get
        {
            yield return new object[] { OrderStatus.PaymentPending, OrderStatus.PaymentApproved };
            yield return new object[] { OrderStatus.PaymentPending, OrderStatus.Cancelled };
            yield return new object[] { OrderStatus.PaymentApproved, OrderStatus.ShippedCarrier };
            yield return new object[] { OrderStatus.PaymentApproved, OrderStatus.Cancelled };
            yield return new object[] { OrderStatus.ShippedCarrier, OrderStatus.Delivered };
        }
    }
}
