﻿using AutoMapper;
using Moq;
using Payment.Application.Orders;
using Payment.Domain.Orders;
using Payment.Domain.Validators;
using Payment.UseCases.UoW;
using System.Collections.ObjectModel;
using Order = Payment.Domain.Orders.Orders;

namespace Payment.Application.Tests.Orders.UseCases;

public class CreateOrderTest
{
    private readonly ICreateOrder _createOrder;
    private readonly Mock<IMapper> _mapper;
    private readonly Mock<IOrderRepository> _orderRepository;
    private readonly Mock<IUnitOfWork> _unitOfWork;

    private static readonly string over100Chars = new('.', 101);
    private static readonly string over16Chars = new('.', 17);
    private static readonly string over11Chars = new('.', 12);
    private static readonly string over250Chars = new('.', 251);
    private static readonly string less100Chars = new('.', 100);
    private static readonly string less16Chars = new('.', 16);
    private static readonly string less11Chars = new('.', 11);
    private static readonly string less250Chars = new('.', 250);

    public CreateOrderTest()
    {
        _orderRepository = new();
        _unitOfWork = new();
        _mapper = new();
        _createOrder = new CreateOrder(_orderRepository.Object, _mapper.Object, _unitOfWork.Object);
    }

    [Fact]
    public async Task CreateOrder_NullInput_ThrowsException()
    {
        var ex = await Assert.ThrowsAsync<OrderException>(() => _createOrder.Execute(null));

        Assert.Single(ex.Errors);
        Assert.Equal(nameof(OrderException), ex.Key);
        Assert.Equal(nameof(OrderError.UninformedData), ex.Errors[0].Key);

        VerifyNoOtherCalls();
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public async Task CreateOrder_MissingRequiredProperties_ThrowsException(bool missingItems)
    {
        var request = missingItems ? RequestWithoutItemsMock : RequestWithoutSellerMock;

        var ex = await Assert.ThrowsAsync<ModelValidationException>(() => _createOrder.Execute(request));

        Assert.Single(ex.Errors);
        Assert.Equal(nameof(ModelValidationException), ex.Key);

        VerifyNoOtherCalls();
    }

    [Theory]
    [MemberData(nameof(Values))]
    public async Task CreateOrder_PropertiesOverMaxLength_ThrowsException(string email, string sellerName, string itemName, string phoneNumber, string document)
    {
        var request = RequestMock;
        request.Seller.Name = sellerName;
        request.Seller.PhoneNumber = phoneNumber;
        request.Seller.Document = document;
        request.Seller.Email = email;
        request.Items.First().Name = itemName;

        var ex = await Assert.ThrowsAsync<ModelValidationException>(() => _createOrder.Execute(request));

        Assert.Single(ex.Errors);
        Assert.Equal(nameof(ModelValidationException), ex.Key);

        VerifyNoOtherCalls();
    }

    [Fact]
    public async Task CreateOrder_AllPropertiesFilled_Succes()
    {
        var dateTimeThatStartedTest = DateTime.UtcNow;

        _orderRepository.Setup(x => x.Save(It.IsAny<Order>())).Verifiable();

        _mapper.Setup(x => x.Map<Order>(It.IsAny<CreateOrderRequest>())).Returns(OrderMapperMock);
        _mapper.Setup(x => x.Map<CreateOrderResponse>(It.IsAny<Order>())).Returns(CreateOrderMapperMock);

        _unitOfWork.Setup(x => x.ExecuteInTransaction(It.IsAny<Func<Task>>()))
            .Returns(async (Func<Task> work) => { await work(); });

        var response = await _createOrder.Execute(RequestMock);

        Assert.Equal(OrderStatus.PaymentPending, response.Status);
        Assert.Equal(RequestMock.Items.First().Name, response.Items.First().Name);
        Assert.Equal(RequestMock.Seller.Name, response.Seller.Name);
        Assert.Equal(RequestMock.Seller.Email, response.Seller.Email);
        Assert.Equal(RequestMock.Seller.Document, response.Seller.Document);
        Assert.Equal(RequestMock.Seller.PhoneNumber, response.Seller.PhoneNumber);

        _orderRepository.Verify(x => x.Save(It.Is<Order>(o =>
                o.Status == OrderStatus.PaymentPending &&
                o.Items.First().Name == RequestMock.Items.First().Name &&
                o.Seller.Name == RequestMock.Seller.Name &&
                o.Seller.Document == RequestMock.Seller.Document &&
                o.Seller.Email == RequestMock.Seller.Email &&
                o.Seller.PhoneNumber == RequestMock.Seller.PhoneNumber &&
                o.CreatedAt.CompareTo(dateTimeThatStartedTest) > 0 &&
                o.UpdatedAt.CompareTo(dateTimeThatStartedTest) > 0
            )), Times.Once);

        _unitOfWork.Verify(x => x.ExecuteInTransaction(It.IsAny<Func<Task>>()), Times.Once);
        _mapper.Verify(x => x.Map<Order>(It.IsAny<CreateOrderRequest>()), Times.Once);
        _mapper.Verify(x => x.Map<CreateOrderResponse>(It.IsAny<Order>()), Times.Once);

        VerifyNoOtherCalls();
    }

    private void VerifyNoOtherCalls()
    {
        _orderRepository.VerifyNoOtherCalls();
        _unitOfWork.VerifyNoOtherCalls();
        _mapper.VerifyNoOtherCalls();
    }

    private static CreateOrderRequest RequestWithoutSellerMock =>
        new()
        {
            Items = ItemsMock,
        };

    private static CreateOrderRequest RequestMock =>
        new()
        {
            Items = ItemsMock,
            Seller = new()
            {
                Document = "123456",
                Email = "teste@teste",
                Name = "Ricardo",
                PhoneNumber = "319999999"
            }
        };

    private static IEnumerable<ItemRequest> ItemsMock =>
        new[]
        {
            new ItemRequest
            {
                Name = "item1"
            },
            new()
            {
                Name= "item2"
            }
        };

    private static CreateOrderRequest RequestWithoutItemsMock =>
        new()
        {
            Items = new Collection<ItemRequest>(),
            Seller = new()
            {
                Document = "123456",
                Email = "teste@teste",
                Name = "Ricardo",
                PhoneNumber = "319999999"
            }
        };

    private static Order OrderMapperMock =>
        new()
        {
            Items = new Collection<Items>()
            {
                new()
                {
                    Name = ItemsMock.First().Name,
                },
                new()
                {
                    Name = ItemsMock.Last().Name,
                }
            },
            Seller = new()
            {
                Document = "123456",
                Email = "teste@teste",
                Name = "Ricardo",
                PhoneNumber = "319999999"
            },
        };

    private static CreateOrderResponse CreateOrderMapperMock =>
        new()
        {
            Items = new Collection<ItemsResponse>()
            {
                new()
                {
                    Name = ItemsMock.First().Name,
                },
                new()
                {
                    Name = ItemsMock.Last().Name,
                }
            },
            Seller = new()
            {

                Document = "123456",
                Email = "teste@teste",
                Name = "Ricardo",
                PhoneNumber = "319999999"
            },
            Status = OrderStatus.PaymentPending
        };

    public static IEnumerable<object[]> Values
    {
        get
        {
            yield return new object[] { over100Chars, less100Chars, less250Chars, less16Chars, less11Chars };
            yield return new object[] { less100Chars, over100Chars, less250Chars, less16Chars, less11Chars };
            yield return new object[] { less100Chars, less100Chars, over250Chars, less16Chars, less11Chars };
            yield return new object[] { less100Chars, less100Chars, less250Chars, over16Chars, less11Chars };
            yield return new object[] { less100Chars, less100Chars, less250Chars, less16Chars, over11Chars };
        }
    }
}
