﻿using AutoMapper;
using Moq;
using Payment.Application.Orders;
using Payment.Domain.Orders;
using Order = Payment.Domain.Orders.Orders;

namespace Payment.Application.Tests.Orders.UseCases;

public class GetOrderByIdTest
{
    private readonly Mock<IMapper> _mapper;
    private readonly Mock<IOrderRepository> _orderRepository;
    private readonly IGetOrderById _getOrderById;

    public GetOrderByIdTest()
    {
        _mapper = new();
        _orderRepository = new();
        _getOrderById = new GetOrderById(_orderRepository.Object, _mapper.Object);
    }

    [Fact]
    public async Task GetOrderById_OrderNotFound_ThrowsException()
    {
        Order? order = null;

        _orderRepository.Setup(x => x.GetOrderById(It.IsAny<Guid>(), It.IsAny<bool>())).ReturnsAsync(order);

        var ex = await Assert.ThrowsAsync<OrderNotFoundException>(() => _getOrderById.Execute(Guid.NewGuid()));

        _orderRepository.Verify(x => x.GetOrderById(It.IsAny<Guid>(), It.IsAny<bool>()), Times.Once);

        VerifyNoOtherCalls();
    }

    [Fact]
    public async Task GetOrderBy_ValidId_Success()
    {
        var id = Guid.NewGuid();

        _orderRepository.Setup(x => x.GetOrderById(It.IsAny<Guid>(), It.IsAny<bool>())).ReturnsAsync(new Order { Id = id });
        _mapper.Setup(x => x.Map<GetOrderByIdResponse>(It.IsAny<Order>())).Returns(new GetOrderByIdResponse { Id = id });

        var response = await _getOrderById.Execute(id);

        Assert.NotNull(response);
        Assert.Equal(id, response.Id);

        _orderRepository.Verify(x => x.GetOrderById(It.IsAny<Guid>(), It.IsAny<bool>()), Times.Once);
        _mapper.Verify(x => x.Map<GetOrderByIdResponse>(It.Is<Order>(o => o.Id == id)), Times.Once);

        VerifyNoOtherCalls();
    }

    private void VerifyNoOtherCalls()
    {
        _orderRepository.VerifyNoOtherCalls();
        _mapper.VerifyNoOtherCalls();
    }
}
