﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Payment.Repositories;

public sealed class DesingTimeDbContextFactory : IDesignTimeDbContextFactory<MarketplaceContext>
{
    public MarketplaceContext CreateDbContext(string[] args)
    {
        if (!Directory.Exists($"{AppContext.BaseDirectory}/Database"))
            Directory.CreateDirectory($"{AppContext.BaseDirectory}/Database");

        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.Development.json", true, true)
            .Build();

        var optionsBuilder = new DbContextOptionsBuilder<MarketplaceContext>();

        optionsBuilder.UseSqlite(string.Format("Data Source={0}/Database/Payment.db", AppContext.BaseDirectory))
            .EnableDetailedErrors()
            .LogTo(action: Log.Warning, minimumLevel: LogLevel.Warning);
        var context = new MarketplaceContext(optionsBuilder.Options);

        return context;
    }
}
