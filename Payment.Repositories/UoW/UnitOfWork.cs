﻿using Payment.UseCases.UoW;

namespace Payment.Repositories.UoW;

internal sealed class UnitOfWork : IUnitOfWork
{
    private readonly MarketplaceContext context;

    public UnitOfWork(MarketplaceContext context)
        => this.context = context ?? throw new ArgumentNullException(nameof(context));

    public async Task ExecuteInTransaction(Func<Task> work)
    {
        using var trans = context.Database.BeginTransaction();

        await work();

        trans.Commit();
    }

    public async Task<T> ExecuteInTransaction<T>(Func<Task<T>> work)
    {
        using var trans = context.Database.BeginTransaction();

        var result = await work();

        trans.Commit();

        return result;
    }
}
