﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Payment.Domain.Orders;

namespace Payment.Repositories.Order;

public class SellerMap : IEntityTypeConfiguration<Sellers>
{
    public void Configure(EntityTypeBuilder<Sellers> builder)
    {
        builder.ToTable(nameof(Sellers));
        builder.HasKey(p => p.Id);
        builder.Property(p => p.Name).HasMaxLength(100).IsRequired();
        builder.Property(p => p.Email).HasMaxLength(100).IsRequired();
        builder.Property(p => p.Document).HasMaxLength(11).IsRequired();
        builder.Property(p => p.PhoneNumber).HasMaxLength(16).IsRequired();
    }
}
