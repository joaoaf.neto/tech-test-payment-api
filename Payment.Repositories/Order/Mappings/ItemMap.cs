﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Payment.Domain.Orders;

namespace Payment.Repositories.Order;

public class ItemMap : IEntityTypeConfiguration<Items>
{
    public void Configure(EntityTypeBuilder<Items> builder)
    {
        builder.ToTable(nameof(Items));
        builder.HasKey(p => p.Id);
        builder.Property(p => p.Name).HasMaxLength(250).IsRequired();
        builder.Property(p => p.OrderId).IsRequired();
        builder.HasOne(p => p.Order).WithMany(p => p.Items).HasForeignKey(p => p.OrderId);
    }
}
