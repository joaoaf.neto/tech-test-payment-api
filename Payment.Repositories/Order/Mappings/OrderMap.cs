﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Payment.Domain.Orders;

namespace Payment.Repositories.Order;

public class OrderMap : IEntityTypeConfiguration<Orders>
{
    public void Configure(EntityTypeBuilder<Orders> builder)
    {
        builder.ToTable(nameof(Orders));
        builder.HasKey(p => p.Id);
        builder.Property(p => p.Status).IsRequired();
        builder.Property(p => p.CreatedAt).IsRequired();
        builder.Property(p => p.UpdatedAt).IsRequired();
        builder.Property(p => p.SellerId).IsRequired();
        builder.HasOne(p => p.Seller).WithMany(p => p.Orders).HasForeignKey(p => p.SellerId);
    }
}
