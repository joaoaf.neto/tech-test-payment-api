﻿using Microsoft.EntityFrameworkCore;
using Payment.Application.Orders;
using Payment.Domain.Orders;
using Payment.Shared.Extensions;

namespace Payment.Repositories.Order;

internal class OrdersRepository : BaseRepository, IOrderRepository
{
    public OrdersRepository(MarketplaceContext context) : base(context) { }

    public async Task<Orders?> GetOrderById(Guid id, bool asNoTracking = true)
    {
        var query = context.Orders.Where(x => x.Id == id)
            .Include(x => x.Seller)
            .Include(x => x.Items).AsQueryable();

        if (asNoTracking)
            query = query.AsNoTracking();

        return await query.FirstOrDefaultAsync();
    }

    public async Task Save(Orders orders)
    {
        if (orders.Id.NoValue())
            await context.AddAsync(orders);

        await context.SaveChangesAsync();
    }
}
