﻿namespace Payment.Repositories;

internal abstract class BaseRepository
{
    protected readonly MarketplaceContext context;

    protected BaseRepository(MarketplaceContext context) 
        => this.context = context ?? throw new ArgumentNullException(nameof(context));
}
