﻿#nullable disable

namespace Payment.Repositories;

public class RepositorySettings
{
    public string ConnectionStrings { get; set; }
}
