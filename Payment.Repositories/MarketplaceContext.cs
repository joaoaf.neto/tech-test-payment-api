﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Orders;

namespace Payment.Repositories;

public class MarketplaceContext : DbContext
{
    public MarketplaceContext(DbContextOptions options) : base(options) { }

    public DbSet<Orders> Orders { get; set; }
    public DbSet<Sellers> Sellers { get; set; }
    public DbSet<Items> Items { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        var assembly = typeof(MarketplaceContext).Assembly;
        modelBuilder.ApplyConfigurationsFromAssembly(assembly);
        base.OnModelCreating(modelBuilder);
    }
}
