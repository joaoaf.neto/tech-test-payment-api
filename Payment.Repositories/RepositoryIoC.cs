﻿using Microsoft.EntityFrameworkCore;
using Payment.Application.Orders;
using Payment.Repositories;
using Payment.Repositories.Order;
using Payment.Repositories.UoW;
using Payment.UseCases.UoW;

namespace Microsoft.Extensions.DependencyInjection;

public static class RepositoryIoC
{
    public static IServiceCollection AddRepositoryIoC(this IServiceCollection services, RepositorySettings repositorySettings)
    {
        if (services is null)
            throw new ArgumentNullException(nameof(services));

        if (repositorySettings == null)
            throw new ArgumentNullException(nameof(repositorySettings));

        services.AddSingleton(repositorySettings);

        services.AddDbContext<MarketplaceContext>(
            options =>
            {
                options.UseSqlite(string.Format(repositorySettings.ConnectionStrings, AppContext.BaseDirectory));
            });

        services.AddScoped<DbContext, MarketplaceContext>();
        services.AddScoped<IUnitOfWork, UnitOfWork>();
        services.AddScoped<IOrderRepository, OrdersRepository>();

        return services;
    }
}
