namespace Payment.Domain.Validators;

public static class EnumExtension
{
    public static bool IsValid(this Enum value)
    {
        var result = Enum.IsDefined(value.GetType(), Convert.ToInt32(value));
        return result;
    }
}